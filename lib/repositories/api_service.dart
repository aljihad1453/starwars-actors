import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_case/models/people_model.dart';
import 'package:test_case/utils/db_utils.dart';

class ApiService {
  var baseUrl = "https://swapi.dev/api/people/?page=2";

  final db = DatabaseHelper.instance;

  Future<String?> getId()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var id = prefs.getString('id_token');
    print("id token: $id");
    return id;
  }

  Future<String?> checkLocal()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var localToken = prefs.getString('local_token');
    print("local_token: $localToken");
    return localToken;
  }

  Future<BaseModel> fetchPeople() async {

    var id = await getId();
    var localToken = await checkLocal();

    var result;
    var resultBody;
    var data;
    var localData;

    try {
      print("local data.. $localData");
      if (localToken == null || localToken.isEmpty) {
        result = await http.get(Uri.parse(baseUrl));
        print("Result from network $result");
        if (result.statusCode != 200) {
          throw Exception("Get People Failed with code: ${result.statusCode}");
        }

        resultBody = jsonDecode(result.body);

        data = resultBody;

        SharedPreferences prefs = await SharedPreferences.getInstance();

        prefs.setString('local_token', "has saved");

        db.insert({
          DatabaseHelper.columnId: id,
          DatabaseHelper.columnData: result.body
        }, DatabaseHelper.dataTable).then((value) => print("Insert data to local success"));
        print("date $data");

      } else {
        localData = await db.queryAModel(id, DatabaseHelper.dataTable);
        data = jsonDecode(localData);
      }

      return BaseModel.fromJson(data);
    } catch (error) {
      return BaseModel.withError(error.toString());
    }
  }
}

final apiService = ApiService();