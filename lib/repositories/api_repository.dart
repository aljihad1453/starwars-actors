import 'package:test_case/models/people_model.dart';
import 'package:test_case/repositories/api_service.dart';

class ApiRepository {
  final _apiService = ApiService();

  Future<BaseModel> fetchPeople(){
    return _apiService.fetchPeople();
  }
}

class NetworkError extends Error {}