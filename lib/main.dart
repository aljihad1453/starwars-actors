import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_case/blocs/auth_bloc/auth_repo.dart';
import 'package:test_case/blocs/widget_bloc.dart';
import 'package:test_case/screens/home_screen.dart';
import 'package:test_case/screens/login_screen.dart';
import 'package:test_case/screens/main_screen.dart';
import 'package:path/path.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  // final database = openDatabase(
  //   // Set the path to the database. Note: Using the `join` function from the
  //   // `path` package is best practice to ensure the path is correctly
  //   // constructed for each platform.
  //   join(await getDatabasesPath(), 'doggie_database.db'),
  // );

  Widget defaultWidget = MainScreen();

  if (prefs.get('id_token') == null) {
    defaultWidget = LoginScreen();
  }

  runApp(MyApp(defaultWidget: defaultWidget,));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Widget? defaultWidget;
  MyApp({this.defaultWidget});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
        primaryColorLight: Colors.grey
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => WidgetBloc()),
        ],
        child: RepositoryProvider(
          create:  (context) => AuthRepository(),
          child: defaultWidget ?? HomeScreen(),
        ),
      ),
    );
  }
}
