import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/people_bloc/people_event.dart';
import 'package:test_case/blocs/people_bloc/people_state.dart';
import 'package:test_case/models/people_model.dart';
import 'package:test_case/repositories/api_repository.dart';
import 'package:test_case/repositories/api_service.dart';

class PeopleBLoc extends Bloc<PeopleEvent, PeopleState> {
  PeopleBLoc() : super(PeopleInitial());


  final ApiRepository _apiRepository = ApiRepository();



  @override
  Stream<PeopleState> mapEventToState(PeopleEvent event) async* {
    // TODO: implement mapEventToState
    if (event is GetPeopleList) {
      try {
        yield PeopleLoading();
        final mList = await _apiRepository.fetchPeople();

        yield PeopleLoaded(mList);

        if (mList.error != null) {
          yield PeopleError(mList.error!);
        }
      } on NetworkError {
        yield PeopleError("Failed fetch data. try agai");
      }
    }
  }

}