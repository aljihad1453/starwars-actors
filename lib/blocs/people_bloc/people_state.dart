import 'package:equatable/equatable.dart';
import 'package:test_case/models/people_model.dart';

abstract class PeopleState extends Equatable {
  const PeopleState();
}

class PeopleInitial extends PeopleState {
  const PeopleInitial();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class PeopleLoading extends PeopleState {
  const PeopleLoading();

  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class PeopleLoaded extends PeopleState {
  final BaseModel? baseModel;
  const PeopleLoaded(this.baseModel);

  @override
  // TODO: implement props
  List<Object?> get props => [baseModel];

}

class PeopleError extends PeopleState {
  final String message;
  const PeopleError(this.message);

  @override
  List<Object> get props => [message];
}