import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/screens/favorite_screen.dart';
import 'package:test_case/screens/home_screen.dart';
import 'package:test_case/screens/profile_screen.dart';

class WidgetBloc extends Bloc<int, Widget> {
  WidgetBloc() : super(HomeScreen());

  int _widgetIndex = 0;
  int get currentIndex => _widgetIndex;

  List<Widget> _listWidgets = [
    HomeScreen(),
    FavoriteScreen(),
    ProfileScreen(),
  ];
  @override
  Stream<Widget> mapEventToState(int event) async* {
    // TODO: implement mapEventToState
    _widgetIndex = event;
    yield _listWidgets.elementAt(_widgetIndex);
  }
}
