
import 'package:test_case/blocs/auth_bloc/form_submission_status.dart';

class SignupState {
  final String username;
  bool get isValidUsername => username.length > 3;

  final String email;
  bool get isValidEmail => email.contains('@');

  final String password;
  bool get isValidPassword => password.length > 6;

  final FormSubmissionStatus formStatus;

  SignupState({this.username = '', this.email = '', this.password = '', this.formStatus = const InitialFormStatus()});

  SignupState copyWith({
    String? username,
    String? email,
    String? password,
    FormSubmissionStatus? formStatus
  }){
    return SignupState(
      username: username ?? this.username,
      email: email ?? this.email,
      password: password ?? this.password,
      formStatus: formStatus ?? this.formStatus,
    );
  }
}