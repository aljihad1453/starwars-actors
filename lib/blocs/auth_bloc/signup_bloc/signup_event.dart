abstract class SignupEvent {}

class SignupUsernameChange extends SignupEvent {
  final String? username;

  SignupUsernameChange({this.username});
}

class SignupEmailChange extends SignupEvent {
  final String? email;

  SignupEmailChange({this.email});
}

class SignupPasswordChange extends SignupEvent {
  final String? password;

  SignupPasswordChange({this.password});
}

class SignupSubmitted extends SignupEvent {}