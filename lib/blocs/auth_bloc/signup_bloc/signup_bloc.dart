import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/auth_bloc/auth_repo.dart';
import 'package:test_case/blocs/auth_bloc/form_submission_status.dart';
import 'package:test_case/blocs/auth_bloc/signup_bloc/signup_event.dart';
import 'package:test_case/blocs/auth_bloc/signup_bloc/signup_state.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  final AuthRepository? authRepo;
  SignupBloc({this.authRepo}) : super(SignupState());

  @override
  Stream<SignupState> mapEventToState(SignupEvent event) async* {
    // TODO: implement mapEventToState
    if (event is SignupUsernameChange) {
      yield state.copyWith(username: event.username);

    } else if (event is SignupEmailChange) {
      yield state.copyWith(email: event.email);

    } else if (event is SignupPasswordChange) {
      yield state.copyWith(password: event.password);

    } else if (event is SignupSubmitted) {
      yield state.copyWith(formStatus: FormSubmitting());

      try {
        await authRepo!.signUp(username: state.username, email: state.email, password: state.password);
        yield state.copyWith(formStatus: SubmissionSuccess());
      } catch (e) {
        yield state.copyWith(formStatus: SubmissionFailed("Error Signup $e"));
      }
    }
  }
}