import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_case/utils/db_utils.dart';

class AuthRepository {

  final DatabaseHelper db = DatabaseHelper.instance;

  Future<dynamic> logIn({required String username, required String password}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("attemping login");
    try {
      List<Map> data = await db.queryARowMultipleArgs(username, password, DatabaseHelper.userTable);
      print("data login ${data.first['id']}");
      var idAsToken = data.first['id'];
      prefs.setString('id_token', idAsToken);
      await Future.delayed(Duration(seconds: 3));
      // var da
      // return data;
    } catch (e) {
      throw Exception("Invalid Login for $username cause $e");
    }
  }

  Future<dynamic> signUp({required String username, required String email, required String password}) async {

    print("data $username, $email, $password");
    try {
      print("inserting data...");
      var result = await db.insert({
        DatabaseHelper.columnId: DateTime.now().toString(),
        DatabaseHelper.columnUsername: username.trim(),
        DatabaseHelper.columnEmail: email.trim(),
        DatabaseHelper.columnPassword: password.trim(),
      }, DatabaseHelper.userTable);
      print("insert data register success...");
      await Future.delayed(Duration(seconds: 3));
      return result;
    } catch (e) {
      throw Exception("Register Failed, try again.");
    }
  }

  Future<bool> signOut()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      prefs.remove('id_token');
    } catch (e) {

    }


    return true;
  }


}