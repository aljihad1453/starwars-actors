import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/auth_bloc/auth_repo.dart';
import 'package:test_case/blocs/auth_bloc/form_submission_status.dart';
import 'package:test_case/blocs/auth_bloc/login_bloc/login_event.dart';
import 'package:test_case/blocs/auth_bloc/login_bloc/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository? authRepo;
  LoginBloc({this.authRepo}) : super(LoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    // TODO: implement mapEventToState
    if (event is LoginUsernameChange) {
      yield state.copyWith(username: event.username);

    } else if (event is LoginPasswordChange) {
      yield state.copyWith(password: event.password);

    } else if (event is LoginSubmitted) {
      yield state.copyWith(formStatus: FormSubmitting());

      try {
        await authRepo!.logIn(username: state.username, password: state.password);
        // print("$data");
        yield state.copyWith(formStatus: SubmissionSuccess());
      } catch (e) {
        yield state.copyWith(formStatus: SubmissionFailed("Error Login $e"));
      }
    }
  }
}