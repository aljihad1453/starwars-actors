import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/profile_bloc/profile_event.dart';
import 'package:test_case/blocs/profile_bloc/profile_state.dart';
import 'package:test_case/models/user_model.dart';
import 'package:test_case/repositories/api_repository.dart';
import 'package:test_case/utils/db_utils.dart';

class ProfileBLoc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBLoc() : super(ProfileInitial());

  // final db = DatabaseHelper.instance;

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    // TODO: implement mapEventToState
    if (event is GetProfile) {
      try {
        yield ProfileLoading();
        // final mData = await db.queryARow("", DatabaseHelper.userTable);
        Future.delayed(Duration(seconds: 3));
        yield ProfileLoaded();

      } on NetworkError {
        yield ProfileError();
      }
    }
  }

}