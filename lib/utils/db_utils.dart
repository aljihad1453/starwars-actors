import 'dart:convert';

import 'package:sqflite/sqflite.dart';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:test_case/models/people_model.dart';

class DatabaseHelper {

  static final _databaseName = "test_maos.db";
  static final _databaseVersion = 1;

  static final userTable = 'users';
  static final dataTable = 'people';

  static final columnId = 'id';
  static final columnUsername = 'username';
  static final columnEmail = 'email';
  static final columnPassword = 'password';

  static final columnData = 'data_people';

  // make this a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  Database? _database;
  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
      version: _databaseVersion,
      onCreate: _onCreate,
    );
  }

  Future<void> deleteDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    try{
      print('deleting db');
      // _database=null;
      deleteDatabase(path);
    }catch(e){
      print(e.toString());
    }
    print('db is deleted');
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE IF NOT EXISTS $userTable (
            $columnId TEXT NOT NULL,
            $columnUsername TEXT NOT NULL,
            $columnEmail TEXT NOT NULL,
            $columnPassword TEXT NOT NULL
          )
          ''');

    await db.execute('''
          CREATE TABLE IF NOT EXISTS $dataTable (
            $columnId TEXT NOT NULL,
            $columnData TEXT
          )
          ''');

  }

  // Helper methods

  // Inserts a row in the database where each key in the Map is a column name
  // and the value is the column value. The return value is the id of the
  // inserted row.
  Future<int> insert(Map<String, dynamic> row, String table) async {
    Database? db = await instance.database;
    return await db!.insert(table, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRows(String table,
      {String? where, List<dynamic>? args}) async {

    Database? db = await instance.database;
    try {
      if(where != null){
        return await db!.query(table, where: where, whereArgs: args);
      } else {
        return await db!.query(table);
      }
    } catch (e) {
      print("Data list $e");
      return [];
    }
  }


  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int?> queryRowCount(String table) async {
    Database? db = await instance.database;
    return Sqflite.firstIntValue(await db!.rawQuery('SELECT COUNT(*) FROM $table'));
  }

  // We are assuming here that the id column in the map is set. The other
  // column values will be used to update the row.
  Future<int> update(Map<String, dynamic> row, String table) async {
    Database? db = await instance.database;
    String id = row[columnId];
    return await db!.update(table, row, where: '$columnId = ?', whereArgs: [id]);
  }

  // Deletes the row specified by the id. The number of affected rows is
  // returned. This should be 1 as long as the row exists.
  Future<int> delete(String id, String table) async {
    Database? db = await instance.database;
    return await db!.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }



  Future<List<Map>>? queryARow(String? id, String table) async {
    Database? db = await instance.database;
    try {
      List<Map> data = await db!.rawQuery('SELECT * FROM $table WHERE $columnUsername = ?', [id]);
      // var jsond = data.first['json_data'];
      print("FROM SQL ${data.map((e) => print(e)).toList()}");
      return data;
    } catch (e) {
      print("error data sql query A Row $e");
      // return
      throw Exception("Yoi gagal");
    }
  }

  Future<String> queryAModel(String? id, String table) async {
    Database? db = await instance.database;
    try {
      var data = await db?.rawQuery('SELECT * FROM $table WHERE $columnId = ?', [id]);
      var json = data?.first['data_people'];
      print("FROM SQL ${data?.map((e) => print(e)).toList()}");
      return json.toString();
    } catch (e) {
      print("error data sql query A Row $e");
      // return
      throw Exception("Yoi gagal");
    }
  }

  Future<List<Map>> queryARowMultipleArgs(String name, String password, String table) async {
    Database? db = await instance.database;
    try {
      List<Map> data = await db!.rawQuery('SELECT * FROM $table WHERE $columnUsername = ? and $columnPassword = ?', [name, password]);
      // var jsond = data.first['json_data'];
      print("FROM SQL ${data.map((e) => print(e)).toList()}");
      return data;
    } catch (e) {
      print("error data sql query A Row $e");
      // return
      throw Exception("Yoi gagal");
    }
  }

  Future close() async {
    Database? db = await instance.database;
    db!.close();
  }
}