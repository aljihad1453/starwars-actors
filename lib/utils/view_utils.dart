import 'package:flutter_bloc/flutter_bloc.dart';

class ViewStyle extends Cubit<bool> {
  ViewStyle() : super(false);

  void changeStyle()=> emit(!state);

}