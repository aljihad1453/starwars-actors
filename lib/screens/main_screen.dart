import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/widget_bloc.dart';

class MainScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var bloc = BlocProvider.of<WidgetBloc>(context);

    return BlocBuilder<WidgetBloc, Widget>(
      builder: (context, state) => Scaffold(
        body: state,
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Beranda",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: "Favorit",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Profil",
            ),
          ],
          currentIndex: bloc.currentIndex,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.black,
          onTap: (index) {
            print("Tsb index $index");
            // _incrementTab(index);
            bloc.add(index);
          },
        ),
      ),
    );
  }
}
