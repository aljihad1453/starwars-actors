import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/people_bloc/people_bloc.dart';
import 'package:test_case/blocs/people_bloc/people_event.dart';
import 'package:test_case/blocs/people_bloc/people_state.dart';
import 'package:test_case/models/people_model.dart';
import 'package:test_case/utils/view_utils.dart';
import 'package:test_case/widgets/decoration.dart';
import 'package:test_case/widgets/grid_tile.dart';
import 'package:test_case/widgets/list_tile.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PeopleBLoc _bLoc = PeopleBLoc();

  List<BaseModel>? items;

  @override
  void initState() {
    // TODO: implement initState
    _bLoc.add(GetPeopleList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PeopleBLoc(),
        ),
        BlocProvider(create: (context) => ViewStyle()),
      ],
      child: Scaffold(
        body: BlocConsumer<PeopleBLoc, PeopleState>(
          bloc: _bLoc,
          listener: (context, state) {
            if (state is PeopleError) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is PeopleInitial) {
              print("Initial....");
              return _buildLoading();
            } else if (state is PeopleLoading) {
              print("Loading...");
              return _buildLoading();
            } else if (state is PeopleLoaded) {
              print("HARUSNYA DISINI");
              return _viewWidget(context, state.baseModel);
            } else if (state is PeopleError) {
              return Container();
            }
            return Container(
              child: Center(
                child: Text("Hmmm..."),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _viewWidget(BuildContext context, BaseModel? baseModel) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(children: [
        SizedBox(
          height: 16,
        ),
        Container(
          margin: EdgeInsets.all(4),
          padding: EdgeInsets.only(bottom: 8, left: 8, top: 8),
          decoration: decoration(context),
          child: Column(
            children: [
              Center(
                child: Text("Star Wars People", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              ),
              SizedBox(
                height: 24,
              ),
              Row(
              children: [
                Expanded(
                  flex: 5,
                  child: _searchField(),
                ),
                Expanded(
                    child: BlocBuilder<ViewStyle, bool>(
                        builder: (context, state) => IconButton(
                            hoverColor: Colors.transparent,
                            enableFeedback: false,
                            onPressed: () =>
                                context.read<ViewStyle>().changeStyle(),
                            icon: state
                                ? Icon(Icons.grid_view)
                                : Icon(Icons.list)))),
              ],
            ),]
          ),
        ),
        Expanded(
          child: BlocBuilder<ViewStyle, bool>(
            builder: (context, state) => state
                ? _buildListCard(context, baseModel)
                : _buildGridCard(context, baseModel),
          ),
        ),
      ]),
    );
  }

  Widget _buildListCard(BuildContext context, BaseModel? model) {
    var data = model?.people;
    return ListView.builder(
      itemCount: data?.length,
      itemBuilder: (context, i) {
        return ListCard(
          id: data?[i].url,
          name: data?[i].name,
          height: data?[i].height,
          mass: data?[i].mass,
          birthYear: data?[i].birth_year,
          gender: data?[i].gender,
        );
      },
    );
  }

  Widget _buildGridCard(BuildContext context, BaseModel? model) {
    var data = model?.people;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1.5 / 2,
          crossAxisSpacing: 8,
          mainAxisSpacing: 10),
      itemCount: data?.length,
      itemBuilder: (ctx, i) => GridCard(
        id: data?[i].url,
        name: data?[i].name,
        height: data?[i].height,
        mass: data?[i].mass,
        birthYear: data?[i].birth_year,
        gender: data?[i].gender,
      ),
    );
  }

  Widget _searchField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 2, 2, 2),
      child: TextFormField(
        decoration: InputDecoration(
            hintText: "Search",
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)))),
        validator: (value) {},
        onChanged: (value) {},
      ),
    );
  }

  // void filterSearchResults(String query) {
  //   List<String> dummySearchList = List<String>();
  //   dummySearchList.addAll(duplicateItems);
  //   if(query.isNotEmpty) {
  //     List<String> dummyListData = List<String>();
  //     dummySearchList.forEach((item) {
  //       if(item.contains(query)) {
  //         dummyListData.add(item);
  //       }
  //     });
  //     setState(() {
  //       items.clear();
  //       items.addAll(dummyListData);
  //     });
  //     return;
  //   } else {
  //     setState(() {
  //       items.clear();
  //       items.addAll(duplicateItems);
  //     });
  //   }
  // }

  Widget _buildLoading() => Center(child: CircularProgressIndicator());
}
