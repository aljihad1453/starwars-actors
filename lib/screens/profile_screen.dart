import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_case/blocs/profile_bloc/profile_bloc.dart';
import 'package:test_case/blocs/profile_bloc/profile_event.dart';
import 'package:test_case/blocs/profile_bloc/profile_state.dart';
import 'package:test_case/repositories/api_service.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final ProfileBLoc _bLoc = ProfileBLoc();

  var id;

  void _getId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    id = prefs.getString('id_token');
  }

  void initState(){
    // TODO: implement initState
    _bLoc.add(GetProfile());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProfileBLoc(),
      child: Scaffold(
        body: BlocConsumer<ProfileBLoc, ProfileState>(
          listener: (context, state) {

          },
          builder: (context, state) {
            if (state is ProfileInitial) {
              print("Initial....");
              return _buildLoading();
            } else if (state is ProfileLoading) {
              print("Loading...");
              return _buildLoading();
            } else if (state is ProfileLoaded) {
              return Container(child: Center(child: Text("User Profile"),),);
            } else if (state is ProfileError) {
              return Container();
            }
            return Container(
              child: Center(
                child: Text("Hmmm..."),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildLoading() => Center(child: CircularProgressIndicator());
}
