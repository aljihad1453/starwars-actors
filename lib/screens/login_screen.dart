import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/auth_bloc/auth_repo.dart';
import 'package:test_case/blocs/auth_bloc/form_submission_status.dart';
import 'package:test_case/blocs/auth_bloc/login_bloc/login_bloc.dart';
import 'package:test_case/blocs/auth_bloc/login_bloc/login_event.dart';
import 'package:test_case/blocs/auth_bloc/login_bloc/login_state.dart';
import 'package:test_case/blocs/widget_bloc.dart';
import 'package:test_case/screens/main_screen.dart';
import 'package:test_case/screens/register_screen.dart';
import 'package:test_case/widgets/text_field.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _keyFormLogin = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title:
            Text("Sign In", style: Theme.of(context).primaryTextTheme.headline6),
        centerTitle: true,
      ),
      body: BlocProvider(
        create: (context) => LoginBloc(
          authRepo: context.read<AuthRepository>(),
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            child: _loginForm(),
          ),
        ),
      ),
    );
  }

  Widget _loginForm() {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        final formStatus = state.formStatus;
        if (formStatus is SubmissionSuccess) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => BlocProvider(create: (context) => WidgetBloc(), child: MainScreen())));
        }
        if (formStatus is SubmissionFailed) {
          _showSnackBar(context, formStatus.exception.toString());
        }
      },
      child: Form(
        key: _keyFormLogin,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 64,
            ),
            _usernameField(),
            _passwordField(),
            _textButton(),
            SizedBox(
              height: 18,
            ),
            _textRegister(),
            SizedBox(
              height: MediaQuery.of(context).size.height / 6,
            ),
          ],
        ),
      ),
    );
  }

  Widget _usernameField() {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
          child: TextFormField(
            decoration: InputDecoration(
              icon: Icon(Icons.person),
              hintText: "Username",
            ),
            validator: (value) =>
                state.isValidUsername ? null : "Invalid username",
            onChanged: (value) => context.read<LoginBloc>().add(
                  LoginUsernameChange(username: value),
                ),
          ),
        );
      },
    );
  }

  Widget _passwordField() {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
          child: TextFormField(
            obscureText: true,
            decoration: InputDecoration(
              icon: Icon(Icons.lock),
              hintText: "Password",
            ),
            validator: (value) =>
                state.isValidPassword ? null : "Invalid password",
            onChanged: (value) => context.read<LoginBloc>().add(
                  LoginPasswordChange(password: value),
                ),
          ),
        );
      },
    );
  }

  Widget _textButton() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
      child: Container(
        color: Colors.black,
        child: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            return state.formStatus is FormSubmitting
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Center(
                      child: TextButton(
                        onPressed: () {
                          if (_keyFormLogin.currentState!.validate()) {
                            context.read<LoginBloc>().add(LoginSubmitted());
                          }
                        },
                        child: Text("Sign In",
                            style:
                                Theme.of(context).primaryTextTheme.headline6),
                      ),
                    ),
                  );
          },
        ),
      ),
    );
  }

  Widget _textRegister() {
    return RichText(
      text: TextSpan(
        text: "Belum punya akun?",
        style: TextStyle(
            fontFamily: "Product-Sans", color: Colors.black54, fontSize: 14),
        children: <TextSpan>[
          TextSpan(
            text: " Register",
            style: TextStyle(
                fontFamily: "Product-Sans",
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.bold),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RegisterScreen()));
                print("Daftar text");
              },
          ),
        ],
      ),
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
