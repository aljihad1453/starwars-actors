import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/blocs/auth_bloc/auth_repo.dart';
import 'package:test_case/blocs/auth_bloc/form_submission_status.dart';
import 'package:test_case/blocs/auth_bloc/signup_bloc/signup_bloc.dart';
import 'package:test_case/blocs/auth_bloc/signup_bloc/signup_event.dart';
import 'package:test_case/blocs/auth_bloc/signup_bloc/signup_state.dart';


class RegisterScreen extends StatefulWidget {

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _keyFormRegister = GlobalKey<FormState>();

  Future<bool> _willPopCallback() async {
    return Future.value(false);
  }


  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => AuthRepository(),
      child: Scaffold(
        body: WillPopScope(
          onWillPop: _willPopCallback,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              title: Text("Daftar",
                  style: Theme.of(context).primaryTextTheme.headline6),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                },
              ),
            ),
            body: BlocProvider(
              create: (context) => SignupBloc(
                authRepo: context.read<AuthRepository>(),
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: _signupForm(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _signupForm() {
    return BlocListener<SignupBloc, SignupState>(
      listener: (context, state){
        final formStatus = state.formStatus;
        if (formStatus is SubmissionSuccess) {
          Navigator.pop(context);
          _showSnackBar(context, "Daftar berhasil, Silahkan masuk...");
        }
        if (formStatus is SubmissionFailed) {
          _showSnackBar(context, formStatus.exception.toString());
        }
      },
      child: Form(
        key: _keyFormRegister,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 64,
            ),
            _usernameField(),
            _emailField(),
            _passwordField(),
            _textButton(),
            SizedBox(
              height: 18,
            ),
            _textRegister(),
            SizedBox(
              height: MediaQuery.of(context).size.height / 6,
            ),
          ],
        ),
      ),
    );
  }

  Widget _usernameField() {
    return BlocBuilder<SignupBloc, SignupState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
          child: TextFormField(
            decoration: InputDecoration(
              icon: Icon(Icons.person),
              hintText: "Username",
            ),
            validator: (value) =>
            state.isValidUsername ? null : "Username is to short",
            onChanged: (value) => context.read<SignupBloc>().add(
              SignupUsernameChange(username: value),
            ),
          ),
        );
      },
    );
  }

  Widget _emailField() {
    return BlocBuilder<SignupBloc, SignupState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
          child: TextFormField(
            decoration: InputDecoration(
              icon: Icon(Icons.mail),
              hintText: "Email",
            ),
            validator: (value) =>
            state.isValidEmail ? null : "Email isn't valid",
            onChanged: (value) => context.read<SignupBloc>().add(
              SignupEmailChange(email: value),
            ),
          ),
        );
      },
    );
  }

  Widget _passwordField() {
    return BlocBuilder<SignupBloc, SignupState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
          child: TextFormField(
            obscureText: true,
            decoration: InputDecoration(
              icon: Icon(Icons.lock),
              hintText: "Password",
            ),
            validator: (value) =>
            state.isValidPassword ? null : "Password is weak",
            onChanged: (value) => context.read<SignupBloc>().add(
              SignupPasswordChange(password: value),
            ),
          ),
        );
      },
    );
  }

  Widget _textButton() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
      child: Container(
        color: Colors.black,
        child: BlocBuilder<SignupBloc, SignupState>(
          builder: (context, state) {
            return state.formStatus is FormSubmitting
                ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
              child: CircularProgressIndicator(),
            ),
                )
                : Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Center(
              child: TextButton(
                  onPressed: () {
                    if (_keyFormRegister.currentState!.validate()) {
                      context.read<SignupBloc>().add(SignupSubmitted());
                    }
                  },
                  child: Text("Register",
                      style: Theme.of(context).primaryTextTheme.headline6),
              ),
            ),
                );
          },
        ),
      ),
    );
  }

  Widget _textRegister() {
    return RichText(
      text: TextSpan(
        text: "Sudah punya akun?",
        style: TextStyle(
            fontFamily: "Product-Sans", color: Colors.black54, fontSize: 14),
        children: <TextSpan>[
          TextSpan(
            text: " Masuk",
            style: TextStyle(
                fontFamily: "Product-Sans",
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.bold),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                Navigator.pop(context);
              },
          ),
        ],
      ),
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}


