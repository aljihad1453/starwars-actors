import 'package:flutter/material.dart';

class GridCard extends StatelessWidget {
  final String? id;
  final String? name;
  final String? height;
  final String? mass;
  final String? birthYear;
  final String? gender;

  const GridCard(
      {Key? key, this.id, this.name, this.height, this.mass, this.birthYear, this.gender})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.only(left: 12, right: 12, top: 12, bottom: 12),
        margin: EdgeInsets.only(left: 6, right: 6, top: 6, bottom: 0),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(6.0),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child:  Stack(
          children : [Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Name", style: TextStyle(fontWeight: FontWeight.w300, fontSize: 9)),
              Text(name ?? " ", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
              SizedBox(height: 2,),
              Text("Height", style: TextStyle(fontWeight: FontWeight.w300, fontSize: 9),),
              Text(height ?? " ", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12),),
              SizedBox(height: 2,),
              Text("Mass", style: TextStyle(fontWeight: FontWeight.w300, fontSize: 9)),
              Text(mass ?? " ", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12),),
              SizedBox(height: 2,),
              Text("Birth Year", style: TextStyle(fontWeight: FontWeight.w300, fontSize: 9)),
              Text(birthYear ?? " ", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12),),
              SizedBox(height: 2,),
              Text("Gender", style: TextStyle(fontWeight: FontWeight.w300, fontSize: 9)),
              Text(gender ?? " ", style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12),),
            ],
          ),
            Positioned(
                right: 2,
                bottom: 2,
                child: IconButton(onPressed: () {}, icon: Icon(Icons.favorite))),
          ]
        )
      )
    );
  }
}
