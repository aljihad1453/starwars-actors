import 'package:flutter/material.dart';

class ListCard extends StatelessWidget {
  final String? id;
  final String? name;
  final String? height;
  final String? mass;
  final String? birthYear;
  final String? gender;

  const ListCard(
      {Key? key,
      this.id,
      this.name,
      this.height,
      this.mass,
      this.birthYear,
      this.gender})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Stack(children: [
      Container(
        padding: EdgeInsets.only(left: 12, right: 12, top: 12, bottom: 12),
        margin: EdgeInsets.only(left: 6, right: 6, top: 6, bottom: 0),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(6.0),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Name"),
                Text("Height"),
                Text("Mass"),
                Text("Birth Year"),
                Text("Gender"),
              ],
            ),
            SizedBox(
              width: 24,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name ?? " ",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  height ?? " ",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                ),
                Text(
                  mass ?? " ",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                ),
                Text(
                  birthYear ?? " ",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                ),
                Text(
                  gender ?? " ",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                ),
              ],
            ),
          ],
        ),
      ),
      Positioned(
        right: 8,
          bottom: 6,
          child: IconButton(onPressed: () {}, icon: Icon(Icons.favorite))),
    ]));
  }
}
