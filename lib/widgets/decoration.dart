import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Decoration decoration(BuildContext context) {
  return BoxDecoration(
    color: Theme.of(context).cardColor,
    borderRadius: const BorderRadius.all(
      Radius.circular(6.0),
    ),
    boxShadow: <BoxShadow>[
      BoxShadow(
        color: Colors.black.withOpacity(0.1),
        blurRadius: 4,
        offset: const Offset(0, 2),
      ),
    ],
  );
}
