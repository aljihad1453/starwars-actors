import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget textField(String label, String hinttext, TextEditingController controller,
    TextInputType typeKeyboard, bool obscuretext, IconData icon, BuildContext context) {
  return Container(
    child: Padding(
        padding: const EdgeInsets.fromLTRB(48, 12, 48, 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(label),
            TextFormField(
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: Colors.black,
                fontFamily: 'Product-Sans',
                letterSpacing: 0.5,
              ),
              controller: controller,
              obscureText: obscuretext,
              validator: (value){
                if (value!.isEmpty){
                  return "$label tidak boleh kosong";
                  // } else if (value.length < 6){
                  //   return "Kami tidak menemukan email tersebut, silahkan coba email lain";
                } else {
                  return null;
                }
                return null;
              },
              cursorColor: Colors.black,
              textInputAction: TextInputAction.next,
              keyboardType: typeKeyboard,
              decoration: InputDecoration(
                  hintText: hinttext,
                  icon: Icon(icon, color: Theme.of(context).primaryColor),
                  isDense: true,
                  focusColor: Colors.black54,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)
                  ),
                  focusedErrorBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.red),
                  ),
                  errorBorder: UnderlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    borderSide: BorderSide(color: Colors.red),
                  ),
                  labelStyle: Theme.of(context).primaryTextTheme.bodyText2

              ),
            ),
          ],
        )
    ),
  );
}