class BaseModel {
    int? count;
    String? next;
    String? previous;
    List<People>? people;
    String? error;
    bool? isFavorite;

    BaseModel({this.count, this.next, this.previous, this.people, this.isFavorite = false});

    BaseModel.withError(String errorMessage) {
        error = errorMessage;
    }

    factory BaseModel.fromJson(Map<String, dynamic> json) {
        return BaseModel(
            count: json['count'], 
            next: json['next'], 
            previous: json['previous'], 
            people: json['results'] != null ? (json['results'] as List).map((i) => People.fromJson(i)).toList() : null,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['next'] = this.next;
        data['previous'] = this.previous;
        if (this.people != null) {
            data['results'] = this.people?.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class People {
    String? birth_year;
    String? created;
    String? edited;
    String? eye_color;
    List<String>? films;
    String? gender;
    String? hair_color;
    String? height;
    String? homeworld;
    String? mass;
    String? name;
    String? skin_color;
    List<String>? species;
    List<String>? starships;
    String? url;
    List<String>? vehicles;

    People({this.birth_year, this.created, this.edited, this.eye_color, this.films, this.gender, this.hair_color, this.height, this.homeworld, this.mass, this.name, this.skin_color, this.species, this.starships, this.url, this.vehicles});

    factory People.fromJson(Map<String, dynamic> json) {
        return People(
            birth_year: json['birth_year'], 
            created: json['created'], 
            edited: json['edited'], 
            eye_color: json['eye_color'], 
            films: json['films'] != null ? new List<String>.from(json['films']) : null, 
            gender: json['gender'], 
            hair_color: json['hair_color'], 
            height: json['height'], 
            homeworld: json['homeworld'], 
            mass: json['mass'], 
            name: json['name'], 
            skin_color: json['skin_color'], 
            species: json['species'] != null ? new List<String>.from(json['species']) : null, 
            starships: json['starships'] != null ? new List<String>.from(json['starships']) : null, 
            url: json['url'], 
            vehicles: json['vehicles'] != null ? new List<String>.from(json['vehicles']) : null, 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['birth_year'] = this.birth_year;
        data['created'] = this.created;
        data['edited'] = this.edited;
        data['eye_color'] = this.eye_color;
        data['gender'] = this.gender;
        data['hair_color'] = this.hair_color;
        data['height'] = this.height;
        data['homeworld'] = this.homeworld;
        data['mass'] = this.mass;
        data['name'] = this.name;
        data['skin_color'] = this.skin_color;
        data['url'] = this.url;
        if (this.films != null) {
            data['films'] = this.films;
        }
        if (this.species != null) {
            data['species'] = this.species;
        }
        if (this.starships != null) {
            data['starships'] = this.starships;
        }
        if (this.vehicles != null) {
            data['vehicles'] = this.vehicles;
        }
        return data;
    }
}